import { TypeImage } from '@/interfaces/fileVariants.interface';
import { IsJSON, IsNumber, IsString } from 'class-validator';

export class CreateFileVariantsDto {
  @IsString()
  public name: TypeImage;

  @IsString()
  public path: string;

  @IsJSON()
  public meta?: { width: string; height: string };
}
