import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateBookDto {
  @IsString()
  public codeVerify: string;

  @IsDate()
  public dateBook: Date;

  @IsNumber()
  public userId: number;

  @IsNumber()
  public dealId: number;
}
