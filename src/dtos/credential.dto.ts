import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class BasicCredentialDto {
  @IsString()
  @IsNotEmpty()
  public username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(50)
  public password: string;
}
