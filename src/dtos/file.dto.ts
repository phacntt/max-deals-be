import { IsNumber, IsString } from 'class-validator';

export class CreateFileDto {
  @IsString()
  public name: string;

  @IsString()
  public path: string;

  @IsString()
  public url: string;

  @IsNumber()
  public size: number;

  @IsString()
  public mimeType: string;
}
