import { IsNumber, IsString } from 'class-validator';

export class CreateStoreDto {
  @IsString()
  public name: string;

  @IsString()
  public slug: string;

  @IsNumber()
  public imageIds?: number[];

  @IsString()
  public description: string;

  @IsString()
  public phone: string;

  @IsString()
  public province: string;

  @IsString()
  public district: string;

  @IsString()
  public ward: string;

  @IsString()
  public street: string;

  @IsString()
  public fullAddress: string;

  @IsNumber()
  public lat: number;

  @IsNumber()
  public lng: number;
}
