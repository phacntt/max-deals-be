import { UserRole } from '@interfaces/users.interface';
import { IsEmail, IsNumber, IsString } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  public email: string;

  @IsString()
  public password: string;

  @IsString()
  public username: string;

  @IsString()
  public name: string;

  @IsString()
  public phone: string;

  @IsString()
  public role: UserRole;

  @IsNumber()
  public storeId: number;
}
