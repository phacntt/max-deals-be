import { FileEntity } from '@entities/file.entity';
import { IsDate, IsNumber, IsString } from 'class-validator';

export class CreateDealDto {
  @IsString()
  public name: string;

  @IsString()
  public slug: string;

  @IsNumber()
  public imageIds: number[];

  @IsDate()
  public expiryDate: Date;

  @IsString()
  public description: string;

  @IsNumber()
  public discount: number;

  @IsNumber()
  public quantity: number;

  @IsNumber()
  public categoryId: number;

  @IsNumber()
  public storeId: number;
}
