import DealController from '@controllers/deal.controller';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { Router } from 'express';

class DealRoute implements Routes {
  public path = '/deals';
  public router = Router();
  public dealController = new DealController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`/`, this.dealController.getDeal);
    this.router.get(`/:id(\\d+)`, this.dealController.getDealById);
    this.router.post(``, authMiddleware, this.dealController.createDeal);
    this.router.put(`/:id(\\d+)`, authMiddleware, this.dealController.updateDeal);
    this.router.delete(`/:id(\\d+)`, authMiddleware, this.dealController.deleteDeal);
  }
}

export default DealRoute;
