import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import BookController from '@controllers/book.controller';
import authMiddleware from '@middlewares/auth.middleware';

class BookRoute implements Routes {
  public path = '/books';
  public router = Router();
  public bookController = new BookController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`/`, authMiddleware, this.bookController.getBooks);
    this.router.get(`/:id(\\d+)`, authMiddleware, this.bookController.getBookById);
    this.router.post(``, authMiddleware, this.bookController.createBook);
    this.router.put(`/:id(\\d+)`, authMiddleware, this.bookController.updateBook);
    this.router.delete(`/:id(\\d+)`, authMiddleware, this.bookController.deleteBook);
  }
}

export default BookRoute;
