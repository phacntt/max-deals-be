import FileController from '@controllers/file.controller';
import { Routes } from '@interfaces/routes.interface';
import { Router } from 'express';
import multer from 'multer';
import path from 'path';
import { UPLOAD_ROOT } from '@/config';

class FileRoute implements Routes {
  public path = '/file';
  public router = Router();
  public fileController = new FileController();

  storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.basename(UPLOAD_ROOT));
    },
    filename: function (req, file, cb) {
      const filename = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, filename + '-' + file.originalname);
    },
  });

  upload = multer({
    storage: this.storage,
    fileFilter: (req, file, cb) => {
      if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        cb(null, false);
        return cb(new Error('Only image files are allowed!'));
      }
      cb(null, true);
    },
  });

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(``, this.fileController.getAllFile);
    this.router.get(`/:id`, this.fileController.getFileByEntityId);
    this.router.get(`/uploads/:nameFile`, this.fileController.getFile);
    this.router.post(`/single`, this.upload.single('formFileMultiple'), this.fileController.createSingleFile);
    this.router.post(`/multiple`, this.upload.array('formFileMultiple', 3), this.fileController.createFile);
    this.router.put(`/:dealId`, this.upload.array('formFileMultiple', 3), this.fileController.updateFile);
    this.router.delete(``, this.fileController.deleteFile);
  }
}

export default FileRoute;
