import StoreController from '@controllers/store.controller';
import { Routes } from '@interfaces/routes.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { Router } from 'express';

class StoresRoute implements Routes {
  public path = '/stores';
  public router = Router();
  public storeController = new StoreController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`/`, this.storeController.getStore);
    this.router.get(`/:id(\\d+)`, this.storeController.getStoreById);
    this.router.post(``, authMiddleware, this.storeController.createStore);
    this.router.put(`/:id(\\d+)`, authMiddleware, this.storeController.updateStore);
    this.router.delete(`/:id(\\d+)`, authMiddleware, this.storeController.deleteStore);
  }
}

export default StoresRoute;
