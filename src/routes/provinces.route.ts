import DealController from '@controllers/deal.controller';
import ProvinceController from '@controllers/province.controller';
import { Routes } from '@interfaces/routes.interface';
import { Router } from 'express';

class ProvinceRoute implements Routes {
  public path = '/provinces';
  public router = Router();
  public provinceController = new ProvinceController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`/`, this.provinceController.getProvince);
    this.router.get(`/:id(\\d+)`, this.provinceController.getProvinceById);
  }
}

export default ProvinceRoute;
