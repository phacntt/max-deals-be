import 'reflect-metadata';
import App from '@/app';
import AuthRoute from '@routes/auth.route';
import IndexRoute from '@routes/index.route';
import UsersRoute from '@routes/users.route';
import validateEnv from '@utils/validateEnv';
import CategoryRoute from './routes/categories.route';
import DealRoute from './routes/deals.route';
import StoresRoute from './routes/stores.route';
import FileRoute from './routes/files.route';
import ProvinceRoute from './routes/provinces.route';
import BookRoute from './routes/book.route';

validateEnv();

const app = new App([
  new IndexRoute(),
  new UsersRoute(),
  new AuthRoute(),
  new CategoryRoute(),
  new DealRoute(),
  new StoresRoute(),
  new FileRoute(),
  new ProvinceRoute(),
  new BookRoute(),
]);

app.listen();

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err);
});
