import Serializer from './base.serializer';

export default class UserSerializer extends Serializer {
  protected defineMappers(): void {
    this.attributes('id', 'username', 'email', 'name', 'role', 'createdAt');
  }
}
