export type MapperFunc = () => any;

export default abstract class Serializer {
  protected attributeSelectors: Set<string> = new Set();

  constructor(protected object: any) {}

  protected abstract defineMappers(): void;

  protected attributes(...fields: string[]) {
    fields.forEach(field => this.attributeSelectors.add(field));
  }

  asJson() {
    this.defineMappers();
    const raw: any = {};

    this.attributeSelectors.forEach(field => {
      raw[field] = this.object.hasOwnProperty(field) ? this.object[field] : this[field]();
    });

    return raw;
  }
}
