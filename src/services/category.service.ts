import { CreateCategoryDto } from '@dtos/category.dto';
import { CategoryEntity } from '@entities/category.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Category } from '@interfaces/category.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, IsNull, Not, Repository, In } from 'typeorm';

export type ConditionDisplayCategory = {
  parentId?: number;
  parentOnly?: string;
  sort?: 'ASC' | 'DESC';
};

@EntityRepository()
class CategoryService extends Repository<CategoryEntity> {
  public async findAllCategory(query?: ConditionDisplayCategory): Promise<Category[]> {
    const condition: any = {};
    if (query.parentId) {
      condition.parentId = query.parentId;
    }
    if (query.parentOnly === 'false') {
      condition.parentId = Not(IsNull());
    }
    if (query.parentOnly === 'true') {
      condition.parentId = IsNull();
    }
    const categories: Category[] = await CategoryEntity.find({
      where: condition,
      relations: ['parentCategory'],
      order: { id: query.sort },
    });
    return categories;
  }

  public async findCategoryById(cateId: number): Promise<Category> {
    if (isEmpty(cateId)) throw new HttpException(400, 'Invalid category ID');
    const findCategory: Category = await CategoryEntity.findOne({ where: { id: cateId }, relations: ['parentCategory'] });
    if (!findCategory) throw new HttpException(404, 'Not found');
    return findCategory;
  }

  public async createCategory(categoryData: CreateCategoryDto): Promise<Category> {
    if (isEmpty(categoryData)) throw new HttpException(400, 'Missing categoryData');
    const findCategory: Category = await CategoryEntity.findOne({ where: [{ name: categoryData.name }, { slug: categoryData.slug }] });

    if (findCategory) throw new HttpException(409, 'Category name or slug existed');

    const createCategory: CategoryEntity = CategoryEntity.create({ ...categoryData });

    if (categoryData.parentId) {
      const checkParentCategory = await CategoryEntity.findOne({ where: { id: categoryData.parentId } });
      if (!checkParentCategory) throw new HttpException(409, 'Category is invalid');

      createCategory.parentCategory = checkParentCategory;
    }

    await createCategory.save();
    CategoryEntity.update(createCategory.id, { slug: `${categoryData.slug}-${createCategory.id}` });

    return createCategory;
  }

  public async updateCategory(cateId: number, categoryData: CreateCategoryDto): Promise<Category> {
    if (isEmpty(categoryData)) throw new HttpException(400, 'Missing categoryData');

    const category: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    if (!category) throw new HttpException(404, 'Category does not exists');

    const existedNameOrSlugCategory = await CategoryEntity.findOne({
      where: [{ name: categoryData.name }, { slug: categoryData.slug }],
    });

    if (existedNameOrSlugCategory && existedNameOrSlugCategory.id !== cateId) throw new HttpException(409, 'Category name or slug existed');

    if (categoryData.parentId) {
      const checkParentCategory = await CategoryEntity.findOne({ where: { id: categoryData.parentId } });
      if (!checkParentCategory) throw new HttpException(409, 'Category is invalid');
    }

    await CategoryEntity.update(category.id, { ...categoryData });
    return this.findCategoryById(cateId);
  }

  public async deleteCategory(cateId: number): Promise<void> {
    if (isEmpty(cateId)) throw new HttpException(400, 'Invalid category ID');

    const findCategory: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    if (!findCategory) throw new HttpException(404, 'Category does not exists');

    await CategoryEntity.delete({ id: cateId });
  }
}

export default CategoryService;
