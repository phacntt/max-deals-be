import { CreateDealDto } from '@dtos/deal.dto';
import { CategoryEntity } from '@entities/category.entity';
import { DealEntity } from '@entities/deal.entity';
import { FileEntity } from '@entities/file.entity';
import { StoreEntity } from '@entities/store.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Deal } from '@interfaces/deal.interface';
import { File } from '@interfaces/file.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, FindOperator, ILike, In, MoreThanOrEqual, Repository } from 'typeorm';
import { FileFilterData } from './file.service';
import { FileVariantsEntity } from '@/entities';
import sharp from 'sharp';
import { CreateFileVariantsDto } from '@/dtos/fileVariants.dto';
import { DOMAIN, UPLOAD_ROOT } from '@/config';
import fs from 'fs';
// const fs = require('fs');

export type ListDealFilter = {
  categoryId?: number;
  storeId?: number;
  expiryDate?: FindOperator<string>;
  limit?: number;
  id?: 'ASC' | 'DESC';
  discount?: 'ASC' | 'DESC';
  name?: FindOperator<string>;
  center_lat?: number;
  center_lng?: number;
  center_radius?: number; // in meter
};

export type SortDeal = {
  'deals.id'?: 'ASC' | 'DESC';
  'deals.discount'?: 'ASC' | 'DESC';
};

@EntityRepository()
class DealService extends Repository<DealEntity> {
  public async listDeals(filter?: ListDealFilter): Promise<Deal[]> {
    const condition: ListDealFilter = {};
    const sortBy: SortDeal = {};

    if (filter?.categoryId) {
      condition.categoryId = filter.categoryId;
    }

    if (filter?.storeId) {
      condition.storeId = filter.storeId;
    }

    if (filter?.expiryDate) {
      condition.expiryDate = MoreThanOrEqual(filter.expiryDate);
    }

    if (filter?.id) {
      sortBy['deals.id'] = filter.id;
    }

    if (filter.discount) {
      sortBy['deals.discount'] = filter.discount;
    }

    if (filter.name) {
      condition.name = ILike(`%${filter.name}%`);
    }

    let queryBuilder = DealEntity.createQueryBuilder('deals')
      .leftJoinAndSelect('deals.category', 'category')
      .leftJoinAndSelect('deals.store', 'store')
      .where(condition)
      .orderBy(sortBy)
      .skip(0)
      .take(filter.limit || 50);

    if (filter.center_lat && filter.center_lng && filter.center_radius) {
      queryBuilder = queryBuilder
        .addSelect(`ST_Distance(geom, ST_MakePoint(${filter.center_lng}, ${filter.center_lat}))`, 'store_distance')
        .andWhere(`ST_DWithin(ST_MakePoint(${filter.center_lng}, ${filter.center_lat}), store.geom, ${Math.min(filter.center_radius, 10000)})`);
    }

    const [deals, total] = await queryBuilder.getManyAndCount();

    return deals;
  }

  public async findDealById(dealId: number): Promise<Deal> {
    if (isEmpty(dealId)) throw new HttpException(400, 'Invalid deal ID');
    const findDeal: Deal = await DealEntity.findOne({ where: { id: dealId }, relations: ['category', 'store'] });
    if (!findDeal) throw new HttpException(409, 'Not found');
    console.log(findDeal);
    return findDeal;
  }

  public async createDeal(dealData: CreateDealDto): Promise<Deal> {
    if (isEmpty(dealData)) throw new HttpException(400, 'Missing dealData');
    const findDeal: Deal = await DealEntity.findOne({ where: { slug: dealData.slug } });
    if (findDeal) throw new HttpException(409, 'Deal slug existed');

    const category = await CategoryEntity.findOne(dealData.categoryId);
    if (!category) throw new HttpException(400, 'Category is invalid');

    const store = await StoreEntity.findOne(dealData.storeId);
    if (!store) throw new HttpException(400, 'Store is invalid');

    const images = await FileEntity.find({ where: { id: In(dealData.imageIds), entityId: null } });
    if (images.length == 0) throw new HttpException(400, 'Images was used or must be at least 1 valid image');

    const validImageIds: number[] = await images.map(img => img.id);

    const listIdFileVariants = [];

    const getPathShort = (str: string, OUTPUT_WIDTH: string, OUTPUT_HEIGHT: string) => {
      return str.slice(0, str.lastIndexOf('/') + 1) + OUTPUT_WIDTH + 'x' + OUTPUT_HEIGHT + '_' + str.slice(str.lastIndexOf('/') + 1);
    };

    for (let i = 0; i < images.length; i++) {
      const metadata = await sharp(images[i].path).metadata();

      const INPUT_WIDTH = metadata.width;
      const INPUT_HEIGHT = metadata.height;
      const OUTPUT_WIDTH = 300;
      const OUTPUT_HEIGHT = ((INPUT_HEIGHT * OUTPUT_WIDTH) / INPUT_WIDTH).toFixed(0);

      const dataSaveThumbnail: CreateFileVariantsDto = {
        name: 'thumbnail',
        path: getPathShort(images[i].path, OUTPUT_WIDTH.toString(), OUTPUT_HEIGHT),
        meta: { width: `${OUTPUT_WIDTH}`, height: `${OUTPUT_HEIGHT}` },
      };
      const newFileVariants = await FileVariantsEntity.create(dataSaveThumbnail);

      newFileVariants.files = images[i];

      await newFileVariants.save();

      await sharp(images[i].path)
        .resize({ width: OUTPUT_WIDTH, fit: 'contain' })
        .toFile(`./uploads/${dataSaveThumbnail.path.slice(dataSaveThumbnail.path.lastIndexOf('/') + 1)}`);

      listIdFileVariants.push(newFileVariants.id);
    }

    const newDeal: DealEntity = await DealEntity.create({ ...dealData });
    newDeal.category = category;
    newDeal.store = store;
    newDeal.images = images;

    await newDeal.save();

    await DealEntity.update(newDeal.id, { slug: `${dealData.slug}-${newDeal.id}` });

    await FileEntity.update(validImageIds, { entityId: newDeal.id, entityType: 'DealEntity' });

    return newDeal;
  }

  public async updateDeal(dealId: number, dealData: CreateDealDto): Promise<Deal> {
    if (isEmpty(dealData)) throw new HttpException(400, 'Missing dealData');

    const findDeal: Deal = await DealEntity.findOne({ where: { id: dealId } });
    if (!findDeal) throw new HttpException(404, 'Deal does not exists');

    const existedNameOrSlugDeal = await DealEntity.findOne({
      where: [{ name: dealData.name }, { slug: dealData.slug }],
    });
    if (existedNameOrSlugDeal && existedNameOrSlugDeal.id !== dealId) throw new HttpException(409, 'Deal name or slug existed');

    await DealEntity.update(dealId, { ...dealData });
    const updateDeal: Deal = await DealEntity.findOne({ where: { id: dealId } });
    return updateDeal;
  }

  public async deleteDeal(dealId: number): Promise<void> {
    if (isEmpty(dealId)) throw new HttpException(400, 'Invalid Deal ID');

    const findDeal: Deal = await DealEntity.findOne({ where: { id: dealId } });
    if (!findDeal) throw new HttpException(404, 'Deal does not exists');

    const findImages = await FileEntity.find({ where: { entityId: dealId } });

    const listIdFile = await findImages.map(image => image.id);
    const findFileVariantImages = await FileVariantsEntity.find({ where: { files: In(listIdFile) } });
    const listIdFileVariantDelete: number[] = await findFileVariantImages.map(img => img.id);

    await FileVariantsEntity.delete({ id: In(listIdFileVariantDelete) });

    await FileEntity.delete({ id: In(listIdFile) });

    for (let i = 0; i < findImages.length; i++) {
      fs.unlinkSync(findImages[i].path);
    }

    for (let i = 0; i < findFileVariantImages.length; i++) {
      fs.unlinkSync(findFileVariantImages[i].path);
    }

    await FileVariantsEntity.delete({ id: In(listIdFileVariantDelete) });

    await DealEntity.delete({ id: dealId });
  }
}

export default DealService;
