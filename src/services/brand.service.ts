import { CreateCategoryDto } from '@dtos/category.dto';
import { CategoryEntity } from '@entities/category.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Category } from '@interfaces/category.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository()
class CategoryService extends Repository<CategoryEntity> {
  public async findAllCategory(): Promise<Category[]> {
    const categories: Category[] = await CategoryEntity.find();
    return categories;
  }

  public async findCategoryById(cateId: number): Promise<Category> {
    if (isEmpty(cateId)) throw new HttpException(400, 'Invalid category ID');
    const findCategory: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    if (!findCategory) throw new HttpException(404, 'Not found');
    return findCategory;
  }

  public async createCategory(categoryData: CreateCategoryDto): Promise<Category> {
    if (isEmpty(categoryData)) throw new HttpException(400, 'Missing categoryData');
    const findCategory: Category = await CategoryEntity.findOne({ where: [{ name: categoryData.name }, { slug: categoryData.slug }] });
    if (findCategory) throw new HttpException(409, 'Category name or slug existed');
    const createCategory: Category = await CategoryEntity.create({ ...categoryData }).save();
    return createCategory;
  }

  public async updateCategory(cateId: number, categoryData: CreateCategoryDto): Promise<Category> {
    if (isEmpty(categoryData)) throw new HttpException(400, 'Missing categoryData');

    const findCategory: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    if (!findCategory) throw new HttpException(404, 'Category does not exists');

    const existedNameOrSlugCategory = await CategoryEntity.findOne({
      where: [{ name: categoryData.name }, { slug: categoryData.slug }],
    });
    if (existedNameOrSlugCategory && existedNameOrSlugCategory.id !== cateId) throw new HttpException(409, 'Category name or slug existed');

    await CategoryEntity.update(cateId, { ...categoryData });
    const updateCategory: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    return updateCategory;
  }

  public async deleteCategory(cateId: number): Promise<void> {
    if (isEmpty(cateId)) throw new HttpException(400, 'Invalid category ID');

    const findCategory: Category = await CategoryEntity.findOne({ where: { id: cateId } });
    if (!findCategory) throw new HttpException(404, 'Category does not exists');

    await CategoryEntity.delete({ id: cateId });
  }
}

export default CategoryService;
