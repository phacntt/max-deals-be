import { CreateStoreDto } from '@dtos/store.dto';
import { DistrictEntity } from '@entities/district.entity';
import { FileEntity } from '@entities/file.entity';
import { ProvinceEntity } from '@entities/province.entity';
import { StoreEntity } from '@entities/store.entity';
import { WardEntity } from '@entities/ward.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Store } from '@interfaces/store.interface';
import { isEmpty } from 'class-validator';
import { BaseEntity, EntityRepository, In, Repository } from 'typeorm';

@EntityRepository()
class StoreService extends Repository<StoreEntity> {
  public async findAllStore(): Promise<Store[]> {
    const stores: Store[] = await StoreEntity.find();
    return stores;
  }

  public async findStoreById(storeId: number): Promise<Store> {
    if (isEmpty(storeId)) throw new HttpException(400, 'StoreId is invalid');
    const findStore: Store = await StoreEntity.findOne({ where: { id: storeId } });
    if (!findStore) throw new HttpException(400, 'Not found');
    return findStore;
  }

  public async createStore(storeData: CreateStoreDto): Promise<Store> {
    if (isEmpty(storeData)) throw new HttpException(400, 'Missing storeData');
    const findStore: Store = await StoreEntity.findOne({ where: { name: storeData.name } });
    if (findStore) throw new HttpException(400, 'Not found cái địt mẹ mày');

    const findCodeProvince = await ProvinceEntity.findOne({ where: { code: storeData.province } });
    if (!findCodeProvince) throw new HttpException(400, 'Not found province');

    const findCodeDistrict = await DistrictEntity.findOne({ where: { code: storeData.district } });
    if (!findCodeDistrict) throw new HttpException(400, 'Not found district');

    const findCodeWard = await WardEntity.findOne({ where: { code: storeData.ward } });
    if (!findCodeWard) throw new HttpException(400, 'Not found ward');

    const images = await FileEntity.find({ where: { id: In(storeData.imageIds), entityId: null } });
    if (!images) throw new HttpException(400, 'Images must be at least 1 valid image');

    const createStore: StoreEntity = StoreEntity.create({ ...storeData });
    createStore.images = images;

    await createStore.save();

    StoreEntity.update(createStore.id, { slug: `${storeData.slug}-${createStore.id}` });

    const validImageIds: number[] = images.map(img => img.id);

    await createStore.save();

    FileEntity.update(validImageIds, { entityId: createStore.id, entityType: 'StoreEntity' });

    return createStore;
  }

  public async updateStore(storeId: number, storeData: CreateStoreDto): Promise<Store> {
    if (isEmpty(storeData)) throw new HttpException(400, 'Missing storeData');

    const findStore: Store = await StoreEntity.findOne({ where: { id: storeId } });
    if (!findStore) throw new HttpException(400, 'Not found');

    const existedNameOrSlugStore: Store = await StoreEntity.findOne({
      where: [{ name: storeData.name }, { slug: storeData.slug }],
    });

    if (existedNameOrSlugStore && existedNameOrSlugStore.id !== storeId) throw new HttpException(409, 'Store name or slug existed');
    await StoreEntity.update(storeId, { ...storeData });
    const updateStore: Store = await StoreEntity.findOne({ where: { id: storeId } });
    return updateStore;
  }

  public async deleteStore(storeId: number): Promise<void> {
    if (isEmpty(storeId)) throw new HttpException(400, 'Not found');
    const findStore: Store = await StoreEntity.findOne({ where: { id: storeId } });

    if (!findStore) throw new HttpException(404, 'Store does not exists');
    const findImages = await FileEntity.find({ where: { entityId: storeId } });

    const listIdDelete: number[] = await findImages.map(img => img.id);

    await FileEntity.delete({ id: In(listIdDelete) });

    await StoreEntity.delete({ id: storeId });
  }
}

export default StoreService;
