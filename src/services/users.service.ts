import { UserEntity } from '@entities/user.entity';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import { hash } from 'bcrypt';
import { EntityRepository, Repository } from 'typeorm';

export type RoleAccount = 'super_admin' | 'admin' | 'user';

export type ListUserFilter = {
  role?: RoleAccount;
  storeId?: number;
};

@EntityRepository()
class UserService extends Repository<UserEntity> {
  public async findAllUser(filter?: ListUserFilter): Promise<User[]> {
    const condition: ListUserFilter = {};

    if (filter?.role) {
      condition.role = filter.role;
    }

    if (filter?.storeId) {
      condition.storeId = filter.storeId;
    }

    const queryBuilder = UserEntity.createQueryBuilder('users').leftJoinAndSelect('users.store', 'store').where(condition).skip(0);
    const [users, total] = await queryBuilder.getManyAndCount();

    return users;
  }

  public async findUserById(userId: number): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, "You're not userId");

    const findUser: User = await UserEntity.findOne({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "You're not user");

    return findUser;
  }

  public async createUser(userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const findUser: User = await UserEntity.findOne({ where: { email: userData.email } });
    if (findUser) throw new HttpException(409, `You're email ${userData.email} already exists`);

    const hashedPassword = await hash(userData.password, 10);
    const createUserData: User = await UserEntity.create({ ...userData, password: hashedPassword }).save();

    return createUserData;
  }

  public async updateUser(userId: number, userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const findUser: User = await UserEntity.findOne({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "You're not user");

    if (userData.password) {
      const hashedPassword = await hash(userData.password, 10);
      await UserEntity.update(userId, { ...userData, password: hashedPassword });
    } else {
      await UserEntity.update(userId, { ...userData });
    }

    const updateUser: User = await UserEntity.findOne({ where: { id: userId } });
    return updateUser;
  }

  public async deleteUser(userId: number): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, "You're not userId");

    const findUser: User = await UserEntity.findOne({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "You're not user");

    await UserEntity.delete({ id: userId });
    return findUser;
  }
}

export default UserService;
