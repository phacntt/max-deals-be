import { ProvinceEntity } from '@entities/province.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Province } from '@interfaces/province.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository()
class ProvinceService extends Repository<ProvinceEntity> {
  public async findAllProvince(): Promise<Province[]> {
    const Province: Province[] = await ProvinceEntity.find();
    return Province;
  }

  public async findProvinceById(province_code: string): Promise<Province> {
    if (isEmpty(province_code)) throw new HttpException(400, 'Province code is invalid');
    const findProvince: Province = await ProvinceEntity.findOne({ where: { code: province_code }, relations: ['districts', 'districts.wards'] });
    if (!findProvince) throw new HttpException(400, 'Not found');
    return findProvince;
  }
}

export default ProvinceService;
