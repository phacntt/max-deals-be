import { CreateBookDto } from '@dtos/book.dto';
import { BookEntity } from '@entities/book.entity';
import { DealEntity } from '@entities/deal.entity';
import { UserEntity } from '@entities/user.entity';
import { HttpException } from '@/exceptions/HttpException';
import { Book } from '@interfaces/book.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, Repository } from 'typeorm';

export type QueryGetBook = {
  userId?: number;
  dealId?: number;
  status?: string;
  codeVerify?: string;
  storeId?: number;
};
@EntityRepository()
class BookService extends Repository<BookEntity> {
  characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  MAX_CODE = 5;
  public async findAllBook(query: QueryGetBook): Promise<Book[]> {
    const condition: QueryGetBook = {};
    if (query.userId) {
      condition.userId = query.userId;
    }

    if (query.dealId) {
      condition.dealId = query.dealId;
    }

    if (query.storeId) {
      condition.storeId = query.storeId;
    }

    if (query.status) {
      condition.status = query.status;
    }

    if (query.codeVerify) {
      condition.codeVerify = query.codeVerify;
    }

    const books: Book[] = await BookEntity.find({ where: condition, relations: ['user', 'deal', 'deal.store'] });
    return books;
  }

  public async findBookById(bookId: number): Promise<Book> {
    if (isEmpty(bookId)) throw new HttpException(400, 'Invalid book ID');
    const findBook: Book = await BookEntity.findOne({ where: { id: bookId }, relations: ['user', 'deal', 'deal.store', 'deal.category'] });
    if (!findBook) throw new HttpException(404, 'Not found');
    return findBook;
  }

  public async createBook(bookData: CreateBookDto): Promise<Book> {
    if (isEmpty(bookData)) throw new HttpException(400, 'Missing bookData');
    const findBook: Book = await BookEntity.findOne({ where: { codeVerify: bookData.codeVerify } });
    if (findBook) {
      let result = '';
      const charactersLength = this.characters.length;
      for (let i = 0; i < this.MAX_CODE; i++) {
        result += this.characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      bookData.codeVerify = result;
    }

    const user = await UserEntity.findOne(bookData.userId);
    if (!user) throw new HttpException(400, 'User is invalid');

    const deal = await DealEntity.findOne(bookData.dealId);
    if (!deal) throw new HttpException(400, 'Deal is invalid');

    const book = await BookEntity.findOne({ where: { userId: bookData.userId, dealId: bookData.dealId } });
    if (book) throw new HttpException(400, 'Each account only gets 1 discount code on 1 deal');

    const quantityCurrent = deal.quantity;

    if (quantityCurrent <= 0) throw new HttpException(400, 'Sold out of discount codes');

    const createBook: BookEntity = BookEntity.create({ ...bookData });
    createBook.user = user;
    createBook.deal = deal;

    await createBook.save();

    await DealEntity.update(bookData.dealId, { quantity: quantityCurrent - 1 });

    return createBook;
  }

  public async updateBook(bookId: number, bookData: CreateBookDto): Promise<Book> {
    if (isEmpty(bookData)) throw new HttpException(400, 'Missing bookData');
    const findBook: Book = await BookEntity.findOne({ where: { id: bookId } });
    if (!findBook) throw new HttpException(404, 'Book does not exists');

    await BookEntity.update(bookId, { ...bookData });
    const updateBook: Book = await BookEntity.findOne({ where: { id: bookId } });
    return updateBook;
  }

  public async deleteBook(bookId: number): Promise<void> {
    if (isEmpty(bookId)) throw new HttpException(400, 'Invalid book ID');
    const findBook: Book = await BookEntity.findOne({ where: { id: bookId } });
    if (!findBook) throw new HttpException(404, 'Book does not exists');
    await BookEntity.delete({ id: bookId });
  }
}

export default BookService;
