import { FileEntity } from '@entities/file.entity';
import { HttpException } from '@/exceptions/HttpException';
import { EntityType, File } from '@interfaces/file.interface';
import { isEmpty } from 'class-validator';
import { EntityRepository, In, Repository } from 'typeorm';
import { FileVariants } from '@/interfaces/fileVariants.interface';
import { FileVariantsEntity } from '@/entities';
export type FileFilterData = {
  name: string;
  path: string;
  size: number;
  mimeType: string;
  entity?: number;
  entityType?: EntityType;
  entityFileVarId?: number;
};

@EntityRepository()
class FileService extends Repository<FileEntity> {
  public async getFile(nameFile: string): Promise<File> {
    if (isEmpty(nameFile)) throw new HttpException(400, 'Invalid nameFile');

    const files: File = await FileEntity.findOne({ where: { name: nameFile } });
    if (!files) throw new HttpException(404, 'Not found');

    return files;
  }

  public async getAllFile(): Promise<File[]> {
    const files: File[] = await FileEntity.find();
    return files;
  }

  public async getFileByEntityId(id: number, typeEntity: EntityType): Promise<FileEntity[]> {
    const files = await FileEntity.find({ where: { entityId: id, entityType: typeEntity }, relations: ['fileVariants'] });

    if (!files || files.length == 0) throw new HttpException(404, 'Not have images');
    return files;
  }

  public async createFile(fileData: FileFilterData): Promise<File> {
    if (isEmpty(fileData)) throw new HttpException(400, 'Missing fileData');
    const createFile: File = await FileEntity.create({ ...fileData }).save();
    return createFile;
  }

  public async updateFile(entityId: number, fileIds: number[], typeUpdate: EntityType): Promise<File> {
    const countExistedFiles = await FileEntity.count({ where: { id: In(fileIds) } });
    if (countExistedFiles !== fileIds.length) throw new HttpException(404, `File does not exists have dealId ${entityId}`);
    await FileEntity.update(fileIds, { entityType: typeUpdate, entityId: entityId });
    const updateFile: File = await FileEntity.findOne({ where: { entityId: entityId } });
    return updateFile;
  }

  public async deleteFile(fileIds: number[]): Promise<void> {
    if (isEmpty(fileIds)) throw new HttpException(400, 'Not found');
    const findFile: File = await FileEntity.findOne({ where: { id: In(fileIds) } });
    if (!findFile) throw new HttpException(404, 'File does not exists');
    await FileEntity.delete(fileIds);
  }
}

export default FileService;
