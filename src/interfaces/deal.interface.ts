import { CategoryEntity } from '@entities/category.entity';
import { FileEntity } from '@entities/file.entity';
import { StoreEntity } from '@entities/store.entity';

export interface Deal {
  id: number;
  name: string;
  slug: string;
  images: FileEntity[];
  expiryDate: Date;
  quantity: number;
  description: string;
  discount: number;
  categoryId: number;
  storeId: number;
  category: CategoryEntity;
  store: StoreEntity;
}
