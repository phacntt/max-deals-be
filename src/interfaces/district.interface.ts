import { ProvinceEntity } from '@entities/province.entity';

export interface District {
  code: string;
  name: string;
  nameEn: string;
  fullName: string;
  fullNameEn: string;
  codeName: string;
  province: ProvinceEntity;
}
