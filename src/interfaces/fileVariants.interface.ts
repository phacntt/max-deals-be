export interface FileVariants {
  id: number;
  name: TypeImage;
  path: string;
  url: string;
  meta: { width: string; height: string };
}

export type TypeImage = 'original' | 'thumbnail';
