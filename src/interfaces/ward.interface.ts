import { DistrictEntity } from '@entities/district.entity';

export interface Ward {
  code: string;
  name: string;
  nameEn: string;
  fullName: string;
  fullNameEn: string;
  codeName: string;
  district: DistrictEntity;
}
