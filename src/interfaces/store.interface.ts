import { FileEntity } from '@/entities';

export interface Store {
  id: number;
  name: string;
  slug: string;
  description: string;
  phone: string;
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAddress: string;
  lat: number;
  lng: number;
  distance: number;
  images: FileEntity[];
}
