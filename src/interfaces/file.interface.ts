import { FileVariantsEntity } from '@/entities/fileVariants.entity';

export interface File {
  id: number;
  name: string;
  path: string;
  url: string;
  size: number;
  mimeType: string;
  entityId: number;
  entityType: EntityType;
  fileVariants: FileVariantsEntity[];
}

export type EntityType = 'DealEntity' | 'CategoryEntity' | 'StoreEntity';
