import { CategoryEntity } from '@entities/category.entity';

export interface Category {
  id: number;
  name: string;
  slug: string;
  parentId: number;
  parentCategory: CategoryEntity;
}
