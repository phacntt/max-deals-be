import { StoreEntity } from '@entities/store.entity';

export interface User {
  id: number;
  username: string;
  password: string;
  email: string;
  phone: string;
  name: string;
  role: UserRole;
  storeId: number;
  store: StoreEntity;
}

export type UserRole = 'super_admin' | 'admin' | 'user';
