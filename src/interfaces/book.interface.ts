import { DealEntity } from '@entities/deal.entity';
import { UserEntity } from '@entities/user.entity';

export interface Book {
  id: number;
  dateBook: Date;
  dateExpiryBook: Date;
  codeVerify: string;
  status: StatusBook;
  userId: number;
  dealId: number;
  user: UserEntity;
}

export type StatusBook = 'Unused' | 'Used' | 'Expired';
