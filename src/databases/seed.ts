import { CategoryEntity } from '@entities/category.entity';
import { ProvinceEntity } from '@entities/province.entity';
import { StoreEntity } from '@entities/store.entity';
import { UserEntity } from '@entities/user.entity';
import { Category } from '@interfaces/category.interface';
import CategoryService from '@services/category.service';
import DealService from '@services/deal.service';
import ProvinceService from '@services/province.service';
import StoreService from '@services/store.service';
import UserService from '@services/users.service';
import path from 'path';
import { createConnection, getManager } from 'typeorm';
import { dbConnection } from '.';
import fs from 'fs';

const runSeed = async () => {
  const conn = await createConnection(dbConnection);

  const userService = new UserService();
  const categoryService = new CategoryService();
  const storeService = new StoreService();
  const provinceService = new ProvinceService();
  const dealService = new DealService();

  // try {
  //   await conn.query(`CREATE EXTENSION postgis;`);
  // } catch (error) {
  //   if (!(error.message as string).includes('extension "postgis" already exists')) {
  //     throw error; // cho nay khong sao dau, do cai roi gio cai lai no quang loi thoi
  //   }
  // }

  if (!(await UserEntity.findOne({ username: 'superadmin' }))) {
    await userService.createUser({
      email: 'superadmin@zamza.vn',
      password: 'admin@123',
      username: 'superadmin',
      name: 'Super Admin',
      phone: '012345678',
      role: 'super_admin',
      storeId: null,
    });
  }

  if (!(await UserEntity.findOne({ username: 'admin' }))) {
    await userService.createUser({
      email: 'admin@zamza.vn',
      password: 'admin@123',
      username: 'admin',
      name: 'Admin',
      phone: '112345678',
      role: 'admin',
      storeId: null,
    });
  }

  if (!(await UserEntity.findOne({ username: 'zamzauser' }))) {
    await userService.createUser({
      email: 'zamza-user@zamza.vn',
      password: '123456',
      username: 'zamzauser',
      name: 'Admin',
      phone: '212345678',
      role: 'user',
      storeId: null,
    });
  }

  let foodCategory: Category = await CategoryEntity.findOne({ name: 'Food' });
  if (!foodCategory) {
    foodCategory = await CategoryEntity.create({
      name: 'Food',
      slug: 'food-1',
    }).save();
  }

  if (!(await CategoryEntity.findOne({ name: 'Lau' }))) {
    await CategoryEntity.create({
      name: 'Lau',
      slug: 'lau-2',
      parentId: foodCategory.id,
    }).save();
  }

  let drinkCategory: Category = await CategoryEntity.findOne({ name: 'Drink' });
  if (!drinkCategory) {
    drinkCategory = await CategoryEntity.create({
      name: 'Drink',
      slug: 'drink-3',
    }).save();
  }

  if (!(await CategoryEntity.findOne({ name: 'Tra Sua' }))) {
    await CategoryEntity.create({
      name: 'Tra Sua',
      slug: 'tra-sua-4',
      parentId: drinkCategory.id,
    }).save();
  }

  // Check if province existed
  if (!(await ProvinceEntity.count({}))) {
    console.log('RUN IMPORT LOCATION');
    await getManager().query(fs.readFileSync(path.join(__dirname, 'location.sql')).toString(), []);
  }

  if (!(await StoreEntity.findOne({ slug: 'the-coffee-house' }))) {
    await StoreEntity.create({
      name: 'The Coffee House',
      slug: 'the-coffee-house',
      description:
        'The Coffee House is a Vietnamese coffeehouse chain, created in 2014. It is based in Ho Chi Minh City. As of March 2018, the chain has over 100 stores across Vietnam that serve over 40,000 customers a day',
      province: '01',
      district: '768',
      ward: '27046',
      phone: '212345678',
      street: '1017/123C Lạc Long Quân',
      fullAddress: '1017/123C Lạc Long Quân, Phường 11, Quận Tân Bình, Thành phố Hồ Chí Minh',
      lat: 10.7878866,
      lng: 106.6472021,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'phuc-long-tea' }))) {
    await StoreEntity.create({
      name: 'Phuc Long',
      slug: 'phuc-long-tea',
      description:
        'After 50 years of experience growing and developing the highest quality of tea leaves and coffee beans coupled with our devotion to providing memorable customer experience, Phuc Long has earned the reputation as a pioneer brand having many creative ideas for the coffee and tea industry.',
      province: '01',
      district: '766',
      ward: '26965',
      phone: '212345678',
      street: '1017/123C Lạc Long Quân',
      fullAddress: '1017/123C Lạc Long Quân, Phường 11, Quận Tân Bình, Thành phố Hồ Chí Minh',
      lat: 10.7433154884,
      lng: 106.612617845,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'coffee-and-tea-saigon' }))) {
    await StoreEntity.create({
      name: 'Coffee and Tea Saigon',
      slug: 'coffee-and-tea-saigon',
      description:
        'After 50 years of experience growing and developing the highest quality of tea leaves and coffee beans coupled with our devotion to providing memorable customer experience, Phuc Long has earned the reputation as a pioneer brand having many creative ideas for the coffee and tea industry.',
      province: '79',
      district: '744',
      ward: '26965',
      phone: '0936043608',
      street: '190 Cao Đạt',
      fullAddress: '190 Cao Đạt P1,Q5, Ho Chi Minh City, Vietnam',
      lat: 10.75437,
      lng: 106.68291,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'mien-dong-thao-coffee-q11' }))) {
    await StoreEntity.create({
      name: 'Miền Đồng Thảo Coffee Q11',
      slug: 'mien-dong-thao-coffee-q11',
      description:
        'After 50 years of experience growing and developing the highest quality of tea leaves and coffee beans coupled with our devotion to providing memorable customer experience, Phuc Long has earned the reputation as a pioneer brand having many creative ideas for the coffee and tea industry.',
      province: '79',
      district: '772',
      ward: '26965',
      phone: '0936043608',
      street: '554E Minh Phụng',
      fullAddress: '554E Minh Phụng . Q11, Ho Chi Minh City, Vietnam',
      lat: 10.7630676408,
      lng: 106.645043743,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'nha-hang-phuong-nam-300' }))) {
    await StoreEntity.create({
      name: 'Nhà Hàng Phương Nam 300',
      slug: 'nha-hang-phuong-nam-300',
      description:
        'After 50 years of experience growing and developing the highest quality of tea leaves and coffee beans coupled with our devotion to providing memorable customer experience, Phuc Long has earned the reputation as a pioneer brand having many creative ideas for the coffee and tea industry.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '0936043608',
      street: '300 Điện Biên Phủ',
      fullAddress: '300 Điện Biên Phủ, Phường 17, Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.799886628276923,
      lng: 106.70694564127149,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'nha-hang-thien-nga' }))) {
    await StoreEntity.create({
      name: 'Nhà Hàng Thiên Nga',
      slug: 'nha-hang-thien-nga',
      description:
        'After 50 years of experience growing and developing the highest quality of tea leaves and coffee beans coupled with our devotion to providing memorable customer experience, Phuc Long has earned the reputation as a pioneer brand having many creative ideas for the coffee and tea industry.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '02838992364',
      street: '653 Bùi Đình Tuý',
      fullAddress: '653 Bùi Đình Tuý, Phường 14, Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.80713723696466,
      lng: 106.69806216518467,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'co-la-quan' }))) {
    await StoreEntity.create({
      name: 'Cò Lả Quán',
      slug: 'co-la-quan',
      description:
        'Đồ ăn ngon, phục vụ tốt, giá cả hợp lý đây là quán rất lâu năm tại khu vực này. Quán theo phong cách chòi lá ngồi ngoài trời, cũng có nhiều phòng điều hòa nhưng khách đa phần là ngồi ở ngoài trời. Do quán lâu năm nên nước sơn và các vật dụng trang trí có phần cũ kĩ. Đồ ăn đa dạng phong phú.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '02835104465',
      street: '55C Huỳnh Đình Hai',
      fullAddress: '55C Huỳnh Đình Hai, Phường 14, Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.804692283834422,
      lng: 106.69954274453247,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'uom-art-hub' }))) {
    await StoreEntity.create({
      name: 'Ươm Art Hub',
      slug: 'uom-art-hub',
      description:
        'Đồ ăn ngon, phục vụ tốt, giá cả hợp lý đây là quán rất lâu năm tại khu vực này. Quán theo phong cách chòi lá ngồi ngoài trời, cũng có nhiều phòng điều hòa nhưng khách đa phần là ngồi ở ngoài trời. Do quán lâu năm nên nước sơn và các vật dụng trang trí có phần cũ kĩ. Đồ ăn đa dạng phong phú.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '0917171197',
      street: '42/58 Hoàng Hoa Thám',
      fullAddress: '42/58 Hoàng Hoa Thám, Phường 7, Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.808949861863699,
      lng: 106.69121716775926,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'banh-uot-bay-hien' }))) {
    await StoreEntity.create({
      name: 'Bánh ướt Bảy Hiền',
      slug: 'banh-uot-bay-hien',
      description:
        'Đồ ăn ngon, phục vụ tốt, giá cả hợp lý đây là quán rất lâu năm tại khu vực này. Quán theo phong cách chòi lá ngồi ngoài trời, cũng có nhiều phòng điều hòa nhưng khách đa phần là ngồi ở ngoài trời. Do quán lâu năm nên nước sơn và các vật dụng trang trí có phần cũ kĩ. Đồ ăn đa dạng phong phú.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '0917171197',
      street: '767 (12 cũ) Lý Thường Kiệt',
      fullAddress: '767 (12 cũ) Lý Thường Kiệt, Phường 11, Tân Bình, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.793084642062896,
      lng: 106.65290920865563,
    }).save();
  }

  if (!(await StoreEntity.findOne({ slug: 'mi-quang-sam' }))) {
    await StoreEntity.create({
      name: 'Mì Quảng Sâm',
      slug: 'mi-quang-sam',
      description:
        'Đồ ăn ngon, phục vụ tốt, giá cả hợp lý đây là quán rất lâu năm tại khu vực này. Quán theo phong cách chòi lá ngồi ngoài trời, cũng có nhiều phòng điều hòa nhưng khách đa phần là ngồi ở ngoài trời. Do quán lâu năm nên nước sơn và các vật dụng trang trí có phần cũ kĩ. Đồ ăn đa dạng phong phú.',
      province: '79',
      district: '765',
      ward: '26965',
      phone: '0917171197',
      street: '8 Ca Văn Thỉnh',
      fullAddress: '8 Ca Văn Thỉnh, Phường 11, Tân Bình, Thành phố Hồ Chí Minh, Việt Nam',
      lat: 10.788958676852447,
      lng: 106.64625046869969,
    }).save();
  }

  await conn.close();

  console.info(`Done`);
};

runSeed();
