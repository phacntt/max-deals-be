import { CreateCategoryDto } from '@dtos/category.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Category } from '@interfaces/category.interface';
import { Deal } from '@interfaces/deal.interface';
import CategoryService from '@services/category.service';
import { Request, Response, NextFunction } from 'express';

class CategoryController {
  public categoryService = new CategoryService();
  public getCategory = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllCategorysData: Category[] = await this.categoryService.findAllCategory(req.query);
      res.status(200).json({ data: findAllCategorysData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getCategoryById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const cateId = Number(req.params.id);
      const findOneCategoryData: Category = await this.categoryService.findCategoryById(cateId);

      res.status(200).json({ data: findOneCategoryData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createCategory = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user' || req.user.role === 'admin') {
        res.status(400).json({ message: "You haven't permision" });
      }
      const categoryData: CreateCategoryDto = req.body;
      const createCategoryData: Category = await this.categoryService.createCategory(categoryData);

      res.status(201).json({ data: createCategoryData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateCategory = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user' || req.user.role === 'admin') {
        res.status(400).json({ message: "You haven't permision" });
      }
      const cateId = Number(req.params.id);
      const categoryData: CreateCategoryDto = req.body;
      const updateCategory: Category = await this.categoryService.updateCategory(cateId, categoryData);

      res.status(201).json({ data: updateCategory, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteCategory = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user' || req.user.role === 'admin') {
        res.status(400).json({ message: "You haven't permision" });
      }
      const cateId = Number(req.params.id);
      await this.categoryService.deleteCategory(cateId);

      res.status(201).json({ message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default CategoryController;
