import { Province } from '@interfaces/province.interface';
import ProvinceService from '@services/province.service';
import { Request, Response, NextFunction } from 'express';

class ProvinceController {
  public provinceService = new ProvinceService();

  public getProvince = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllProvinceData: Province[] = await this.provinceService.findAllProvince();
      res.status(200).json({ data: findAllProvinceData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getProvinceById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const province_code = req.params.id;
      const findOneProvinceData: Province = await this.provinceService.findProvinceById(province_code);

      res.status(200).json({ data: findOneProvinceData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };
}

export default ProvinceController;
