import { CreateBookDto } from '@dtos/book.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Book } from '@interfaces/book.interface';
import BookService, { QueryGetBook } from '@services/book.service';
import { Request, Response, NextFunction } from 'express';

class BookController {
  public bookService = new BookService();
  public getBooks = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllBooksData: Book[] = await this.bookService.findAllBook(req.query);
      res.status(200).json({ data: findAllBooksData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getBookById = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const bookId = Number(req.params.id);
      const findOneBookData: Book = await this.bookService.findBookById(bookId);

      res.status(200).json({ data: findOneBookData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createBook = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const bookData: CreateBookDto = req.body;
      const createBookData: Book = await this.bookService.createBook(bookData);

      res.status(201).json({ data: createBookData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateBook = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const bookId = Number(req.params.id);
      const bookData: CreateBookDto = req.body;
      const updateBook: Book = await this.bookService.updateBook(bookId, bookData);

      res.status(201).json({ data: updateBook, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteBook = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const bookId = Number(req.params.id);
      await this.bookService.deleteBook(bookId);

      res.status(201).json({ message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default BookController;
