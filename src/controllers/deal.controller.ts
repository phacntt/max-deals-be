import { CreateDealDto } from '@dtos/deal.dto';
import { FileEntity } from '@entities/file.entity';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Deal } from '@interfaces/deal.interface';
import CategoryService from '@services/category.service';
import DealService from '@services/deal.service';
import FileService from '@services/file.service';
import { Request, Response, NextFunction } from 'express';
import { In } from 'typeorm';

class DealController {
  public dealService = new DealService();
  public categoryService = new CategoryService();
  public fileService = new FileService();

  public getDeal = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const deals: Deal[] = await this.dealService.listDeals(req.query);
      const listDealIds = deals.map(deal => deal.id);
      const images = await FileEntity.find({ where: { entityId: In(listDealIds) }, relations: ['fileVariants'] });
      const dealsResp = deals.map(deal => {
        const imageByEntityId = images.filter(image => image.entityId === deal.id);
        deal.images = imageByEntityId;
        return deal;
      });

      res.status(200).json({ data: dealsResp, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getDealById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const dealId = Number(req.params.id);
      const findOneDealData: Deal = await this.dealService.findDealById(dealId);
      const listImages = await this.fileService.getFileByEntityId(findOneDealData.id, 'DealEntity');
      findOneDealData.images = listImages;

      res.status(200).json({ data: findOneDealData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createDeal = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user') {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const dealData: CreateDealDto = req.body;
      if (req.user.role === 'admin' && req.user.storeId != dealData.storeId) {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const createDealData: Deal = await this.dealService.createDeal(dealData);
      res.status(201).json({ data: createDealData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateDeal = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user') {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const dealId = Number(req.params.id);
      const dataReq = req.body;
      if (req.user.role === 'admin' && req.user.storeId != dataReq.storeId) {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const listFileIds = await this.fileService.getFileByEntityId(dealId, 'DealEntity');

      const availableIds = listFileIds.map(image => image.id);
      const currentIds = dataReq.imageIds;

      // List Id File need Delete
      const fileIdsDelete: number[] = availableIds.filter(x => currentIds.indexOf(x) === -1);

      // List Id File need Update
      const fileIdsUpdate: number[] = currentIds.filter(x => availableIds.indexOf(x) === -1);

      if (fileIdsUpdate.length != 0) {
        await this.fileService.updateFile(dealId, fileIdsUpdate, 'DealEntity');
      }

      if (fileIdsDelete.length != 0) {
        await this.fileService.deleteFile(fileIdsDelete);
      }

      const updateDeal: Deal = await this.dealService.updateDeal(dealId, dataReq.dealData);

      res.status(201).json({ data: updateDeal, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteDeal = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user') {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const dealId = Number(req.params.id);
      const dealData = req.body;
      if (req.user.role === 'admin' && req.user.storeId != dealData.storeId) {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      await this.dealService.deleteDeal(dealId);

      res.status(201).json({ message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default DealController;
