import { FileEntity } from '@/entities';
import { CreateStoreDto } from '@dtos/store.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Store } from '@interfaces/store.interface';
import DealService from '@services/deal.service';
import FileService from '@services/file.service';
import StoreService from '@services/store.service';
import { Request, Response, NextFunction } from 'express';
import { In } from 'typeorm';

class StoreController {
  public storeService = new StoreService();
  public dealService = new DealService();
  public fileService = new FileService();

  public getStore = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllStoresData: Store[] = await this.storeService.findAllStore();
      const listStoreIds = findAllStoresData.map(store => store.id);
      const images = await FileEntity.find({ where: { entityId: In(listStoreIds) }, relations: ['fileVariants'] });
      const storesResp = findAllStoresData.map(store => {
        const imageByEntityId = images.filter(image => image.entityId === store.id);
        store.images = imageByEntityId;
        return store;
      });
      res.status(200).json({ data: storesResp, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getStoreById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const storeId = Number(req.params.id);
      const findOneStoreData: Store = await this.storeService.findStoreById(storeId);
      const listImages = await this.fileService.getFileByEntityId(findOneStoreData.id, 'StoreEntity');
      findOneStoreData.images = listImages;
      res.status(200).json({ data: findOneStoreData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createStore = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user' || req.user.role === 'admin') {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }

      const storeData: CreateStoreDto = req.body;

      const createStoreData: Store = await this.storeService.createStore(storeData);

      res.status(201).json({ data: createStoreData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateStore = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user') {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const storeId = Number(req.params.id);
      const dataReq = req.body;
      if (req.user.storeId !== dataReq.storeId) {
        res.status(403).json({ message: "You haven't permision" });
        return;
      }
      const listFileIds = await this.fileService.getFileByEntityId(storeId, 'StoreEntity');

      const availableIds = listFileIds.map(image => image.id);
      const currentIds = dataReq.imageIds;

      // List Id File need Delete
      const fileIdsDelete: number[] = availableIds.filter(x => currentIds.indexOf(x) === -1);

      // List Id File need Update
      const fileIdsUpdate: number[] = currentIds.filter(x => availableIds.indexOf(x) === -1);

      if (fileIdsUpdate.length != 0) {
        await this.fileService.updateFile(storeId, fileIdsUpdate, 'StoreEntity');
      }

      if (fileIdsDelete.length != 0) {
        await this.fileService.deleteFile(fileIdsDelete);
      }

      const updateStore: Store = await this.storeService.updateStore(storeId, dataReq.storeData);

      res.status(201).json({ data: updateStore, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteStore = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.user.role === 'user' || req.user.role === 'admin') {
        res.status(400).json({ message: "You haven't permision" });
      }
      const storeId = Number(req.params.id);
      const dataReq = req.body;

      const deleteStore: void = await this.storeService.deleteStore(storeId);

      res.status(201).json({ message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default StoreController;
