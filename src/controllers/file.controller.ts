import { DOMAIN, UPLOAD_ROOT } from '@/config';
import { FileEntity } from '@/entities';
import { EntityType, File } from '@interfaces/file.interface';
import FileService, { FileFilterData } from '@services/file.service';
import { Request, Response, NextFunction } from 'express';

class FileController {
  public fileService = new FileService();

  public getFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const name = req.params;
      const path: string = UPLOAD_ROOT;

      res.status(200).sendFile(name.nameFile, { root: path });
    } catch (error) {
      next(error);
    }
  };

  public getFileByEntityId = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data = req.query;
      const files: FileEntity[] = await this.fileService.getFileByEntityId(Number(data.id), data.typeEntity as EntityType);
      const filess = files.map(image => image.getURL);
      console.log(filess);
      res.status(200).json({ data: files, message: 'find file by ID' });
    } catch (error) {
      next(error);
    }
  };

  public getAllFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const files: File[] = await this.fileService.getAllFile();
      res.status(200).json({ data: files, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public createFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const files = req.files;
      if (!files) {
        res.status(201).json({ message: 'Cannot find file' });
      }

      let dataSave: FileFilterData;
      const array: File[] = [];
      for (let i = 0; i < files.length; i++) {
        dataSave = {
          name: files[i].filename,
          path: files[i].path.replaceAll('\\', '/'),
          size: files[i].size,
          mimeType: files[i].mimetype,
        };
        const data: File = await this.fileService.createFile(dataSave);
        array.push(data);
      }

      res.status(201).json({ data: array, message: 'created' });
    } catch (error) {
      next(error);
    }
  };
  public createSingleFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const file = req.file;
      const dataImage = req.body;
      if (!file) {
        res.status(201).json({ message: 'Cannot find file' });
      }

      const dataSave: FileFilterData = {
        name: file.filename,
        path: file.path.replace(/\\/g, '/'),
        size: file.size,
        mimeType: file.mimetype,
        entityFileVarId: parseInt(dataImage.entityFileVarId),
      };
      console.log(file);
      console.log(dataImage);

      const data: File = await this.fileService.createFile(dataSave);

      res.status(201).json({ data: data, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const dealId = Number(req.params.dealId);
      const fileIds = req.body;
      if (!fileIds) {
        res.status(201).json({ message: 'Cannot find file' });
      }

      for (let i = 0; i < fileIds.length; i++) {
        await this.fileService.updateFile(dealId, fileIds[i], 'DealEntity');
      }

      res.status(201).json({ message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const fileIds = req.body;
      for (let i = 0; i < fileIds.length; i++) {
        await this.fileService.deleteFile(fileIds[i]);
      }

      res.status(201).json({ message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}
export default FileController;
