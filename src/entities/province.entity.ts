import { Province } from '@interfaces/province.interface';
import { BaseEntity, Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { DistrictEntity } from '.';

@Entity({ name: 'provinces' })
export class ProvinceEntity extends BaseEntity implements Province {
  @PrimaryColumn()
  code: string;

  @Column()
  name: string;

  @Column({ name: 'name_en' })
  nameEn: string;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'full_name_en' })
  fullNameEn: string;

  @Column({ name: 'code_name' })
  codeName: string;

  @OneToMany(() => DistrictEntity, district => district.province)
  districts: DistrictEntity;
}
