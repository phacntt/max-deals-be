import { Store } from '@interfaces/store.interface';
import { BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { DealEntity, FileEntity } from '.';

@Entity({ name: 'stores' })
export class StoreEntity extends BaseEntity implements Store {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  slug: string;

  images: FileEntity[];

  @Column({
    nullable: true,
  })
  description: string;

  @Column({
    nullable: true,
  })
  phone: string;

  @Column()
  province: string;

  @Column()
  district: string;

  @Column()
  ward: string;

  @Column()
  street: string;

  @Column()
  fullAddress: string;

  @Column({
    type: 'double precision',
  })
  lat: number;

  @Column({
    type: 'double precision',
  })
  lng: number;

  @Column('double precision', { select: false, nullable: true })
  distance: number;

  @OneToMany(() => DealEntity, deal => deal.store)
  deal: DealEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
