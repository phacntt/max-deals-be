import { District } from '@interfaces/district.interface';
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { ProvinceEntity, WardEntity } from '.';

@Entity({ name: 'districts' })
export class DistrictEntity extends BaseEntity implements District {
  @PrimaryColumn()
  code: string;

  @Column()
  name: string;

  @Column({ name: 'name_en' })
  nameEn: string;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'full_name_en' })
  fullNameEn: string;

  @Column({ name: 'code_name' })
  codeName: string;

  @ManyToOne(() => ProvinceEntity, province => province.code)
  province: ProvinceEntity;

  @OneToMany(() => WardEntity, ward => ward.district)
  wards: WardEntity;
}
