import { DOMAIN } from '@/config';
import { FileVariants, TypeImage } from '@/interfaces/fileVariants.interface';
import { IsNotEmpty } from 'class-validator';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, AfterLoad } from 'typeorm';
import { FileEntity } from './file.entity';

@Entity({ name: 'fileVariants' })
export class FileVariantsEntity extends BaseEntity implements FileVariants {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'original', type: 'varchar', length: 15 })
  @IsNotEmpty()
  name: TypeImage;

  @Column()
  @IsNotEmpty()
  path: string;

  url: string;

  @Column('jsonb', { nullable: true })
  meta: { width: string; height: string };

  @ManyToOne(() => FileEntity, files => files.fileVariants)
  files: FileEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @AfterLoad()
  getURL() {
    if (this.path !== '') this.url = DOMAIN + this.path;
  }
}
