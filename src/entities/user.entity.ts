import { IsNotEmpty } from 'class-validator';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { User, UserRole } from '@interfaces/users.interface';
import { BookEntity, StoreEntity } from '.';

@Entity('users')
export class UserEntity extends BaseEntity implements User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @Index({ unique: true })
  email: string;

  @Column()
  @IsNotEmpty()
  password: string;

  @Column()
  @IsNotEmpty()
  @Index({ unique: true })
  username: string;

  @Column()
  @IsNotEmpty()
  name: string;

  @Column()
  @IsNotEmpty()
  phone: string;

  @Column({
    default: 'user',
    type: 'varchar',
    length: 15,
  })
  @IsNotEmpty()
  role: UserRole;

  @Column({ nullable: true })
  storeId: number;

  @OneToOne(() => StoreEntity)
  @JoinColumn()
  store: StoreEntity;

  @OneToMany(() => BookEntity, book => book.user)
  book: BookEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
