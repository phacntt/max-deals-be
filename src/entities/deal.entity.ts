import { Deal } from '@interfaces/deal.interface';
import { IsNotEmpty } from 'class-validator';
import { BaseEntity, Column, CreateDateColumn, Entity, Generated, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BookEntity, CategoryEntity, FileEntity, StoreEntity } from '.';

@Entity({ name: 'deals' })
export class DealEntity extends BaseEntity implements Deal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  name: string;

  @Column()
  @IsNotEmpty()
  slug: string;

  images: FileEntity[];

  @Column()
  @IsNotEmpty()
  expiryDate: Date;

  @Column()
  quantity: number;

  @Column()
  @IsNotEmpty()
  description: string;

  @Column()
  @IsNotEmpty()
  discount: number;

  @Column()
  @IsNotEmpty()
  categoryId: number;

  @Column()
  @IsNotEmpty()
  storeId: number;

  @ManyToOne(() => CategoryEntity, category => category.id)
  category: CategoryEntity;

  @ManyToOne(() => StoreEntity, store => store.id)
  store: StoreEntity;

  @OneToMany(() => BookEntity, book => book.deal)
  book: BookEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
