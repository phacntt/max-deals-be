import { Book } from '@interfaces/book.interface';
import { IsNotEmpty } from 'class-validator';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { DealEntity, UserEntity } from '.';

@Entity({ name: 'books' })
export class BookEntity extends BaseEntity implements Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  dateBook: Date;

  @Column()
  @IsNotEmpty()
  dateExpiryBook: Date;

  @Column()
  @IsNotEmpty()
  codeVerify: string;

  @Column({ default: 'Unused' })
  @IsNotEmpty()
  status: 'Unused' | 'Used' | 'Expired';

  @Column()
  @IsNotEmpty()
  userId: number;

  @Column()
  @IsNotEmpty()
  dealId: number;

  @ManyToOne(() => UserEntity)
  user: UserEntity;

  @ManyToOne(() => DealEntity)
  deal: DealEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
