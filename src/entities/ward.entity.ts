import { Ward } from '@interfaces/ward.interface';
import { BaseEntity, Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { DistrictEntity } from '.';

@Entity({ name: 'wards' })
export class WardEntity extends BaseEntity implements Ward {
  @PrimaryColumn()
  code: string;

  @Column()
  name: string;

  @Column({ name: 'name_en' })
  nameEn: string;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'full_name_en' })
  fullNameEn: string;

  @Column({ name: 'code_name' })
  codeName: string;

  @ManyToOne(() => DistrictEntity, districts => districts.code)
  district: DistrictEntity;
}
