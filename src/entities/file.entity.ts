import { DOMAIN } from '@/config';
import { File } from '@interfaces/file.interface';
import { IsNotEmpty } from 'class-validator';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, AfterLoad } from 'typeorm';
import { FileVariantsEntity } from './fileVariants.entity';

@Entity({ name: 'files' })
export class FileEntity extends BaseEntity implements File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  name: string;

  @Column()
  @IsNotEmpty()
  path: string;

  url: string;

  @Column()
  @IsNotEmpty()
  size: number;

  @Column()
  @IsNotEmpty()
  mimeType: string;

  @Column({
    nullable: true,
  })
  entityId: number;

  @Column({ default: 'DealEntity' })
  @IsNotEmpty()
  entityType: 'DealEntity' | 'CategoryEntity' | 'StoreEntity';

  @OneToMany(() => FileVariantsEntity, fileVariants => fileVariants.files)
  fileVariants: FileVariantsEntity[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @AfterLoad()
  getURL() {
    if (this.path !== '') this.url = DOMAIN + this.path;
  }
}
