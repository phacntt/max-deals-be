import { Category } from '@interfaces/category.interface';
import { IsNotEmpty } from 'class-validator';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DealEntity } from '.';

@Entity({ name: 'categories' })
export class CategoryEntity extends BaseEntity implements Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @Index({ unique: true })
  name: string;

  @Column()
  @IsNotEmpty()
  @Index({ unique: true })
  slug: string;

  @Column({
    nullable: true,
  })
  parentId: number;

  @OneToMany(() => DealEntity, deal => deal.category)
  deal: DealEntity;

  @ManyToOne(() => CategoryEntity)
  @JoinColumn({ name: 'parent_id' })
  parentCategory: CategoryEntity;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}
