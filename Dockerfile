FROM node:16.13.0-alpine3.12

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

RUN npm run build:tsc

CMD [ "node", "dist/server.js" ]