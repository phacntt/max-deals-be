# Setup
## 1. Prequesites
- Node 16.x
- Docker
- Docker Compose
- Tested on Node v14.17.0, npm 6.14.13

## 2. Install dependences
- `npm install`

## 3. Init database
- Start PostgreSQL database (make sure the Docker engine is running)
```
docker-compose up -d pg
```
Please check the database username, password from `docker-compose.yml`
- Init schema: `npm run db:sync`
- Use database management tool (DBeaver) and verify if the database was created correctly

# Development
## 1. VScode
- Open "Run and Debug", select the "Dev typescript-express-starter" the click on the start debugging button or press F5

## 2. Add a new migration
- Run `npm run add:migration [file_name]`, ex: `npm run add:migration AddCategoryTable`
- Refer to https://typeorm.io/#/migrations to learn how to create a migration
- After adding migration file, run `npm run db:migrate` to execute the migration file

## 3. Test API
- Open `auth.http` file, modify the parameters then click on the "Send request" button

# Deployment
## 1. Start PostgreSQL database
- Copy `docker/db/docker-compose.yml` to `docker-compose.db.yml` in root project
- Modify db secrets: user, password, db name
- Start db
```
cd docker/db
docker-compose -f docker-compose.db.yml up -d
```
## 2. Create .env
- Copy `.env.development.local` to `.env.production`
- Modify database secret vars as #1

## 3. Init database
- Create schema and tables
```
npx cross-env NODE_ENV=production npm run db:sync
```
- Init some data
```
npx cross-env NODE_ENV=production npm run db:seed
```

## 4. Build and start server
- Use `pm2` to run server
```
npm run deploy:prod
```
- View `pm2` log
```
npx pm2 log
```
- Open `http/auth.http` then execute an API to confirm the server is working

# Deployment with Docker
## 1. Start PostgreSQL database
- Run command:
```
docker-compose up -d pg
```
## 2. Database initialization
- Run command:
```
docker-compose run --rm be sh
```
- Then, run command to init tables:
```
npm run db:sync 
```
- After, run command to init data:

```
npm run db:seed 
```

## 3. Start Backend
- Start server BE with command:
```
docker-compose up -d be
```

- Open `http/auth.http` then execute an API to confirm the server is working